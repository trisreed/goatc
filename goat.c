// Goat-C Compiler and Decompiler.
// (c) Tristan Reed, 2015.
// Released under the WTFPL.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Print Help
// Outputs the generic help strings
void printHelp() {
        printf("Usage: goatc [SWITCH] [INPUT] [OUTPUT]\n");
        printf("------------------------------\n");
        printf("-i\tConvert your C code into Goat-C. Must specify input and output.\n");
        printf("-o\tConvert your Goat-C into C. Must specify input and output.\n");
        printf("-r\tRun your Goat-C code directly. Output is ignored but required.\n");
        // printf("Please use the command-line switches to operate.\n");
        // printf("-i filename.c\tConvert your C code into Goat-C\n");
        // printf("-o filename.e03\t")
}

// C to Goatse
// Converts C into Goat-C
void cToGoatse(char* input, char* output) {
    // Declare vars to read in the C to an array
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    // Attempt to open input file
    fp = fopen(input, "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    // Read file line-by line
    while ((read = getline(&line, &len, fp)) != -1) {
        // For each character in the line
        for (int i=0; i<read; i++) {
            // Define currentChar and thisChar variable
            // char currentChar;
            char thisChar;
            // Convert the character into binary
            // itoa(&line[i], thisChar, 2);
            sprintf(&thisChar, "%d", &line[i]);
            // Test
            printf("%c", thisChar);
        }
       printf("Retrieved line of length %zu :\n", read);
       printf("%s", line);
    }

    // Close and free data
    fclose(fp);
    if (line)
       free(line);
    exit(EXIT_SUCCESS);
}

// Goatse to C
// Converts Goat-C to C
void goatseToC(char* input, char* output) {

}

// Run Goatse
// Runs Goat-C
void runGoatse(char* input) {

}

// Main Function
// Runs the applicable sub-routine for the chosen switch
int main(int argc, char *argv[]) {
    printf("Welcome to the Goat-C Interpreter!\n");
    if (argc != 3) {
        printHelp();
    } else {
        if (strcmp(argv[0], "-i") == 1) {
            cToGoatse(argv[1], argv[2]);
        } else if (strcmp(argv[0], "-o") == 1) {
            goatseToC(argv[1], argv[2]);
        } else if (strcmp(argv[0], "-i") == 1) {
            runGoatse(argv[1]);
        } else {
            printHelp();
        }
    }
    return 0;
}